# Generated by Django 4.0.3 on 2023-06-06 19:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0004_alter_sale_price'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sale',
            old_name='sales_person',
            new_name='salesperson',
        ),
    ]
