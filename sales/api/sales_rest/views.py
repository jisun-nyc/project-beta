from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import requests

from .encoders import (
    SalespeopleListEncoder,
    CustomersListEncoder,
    SalesListEncoder,
    )

from .models import (
    Sale,
    Salesperson,
    Customer,
    AutomobileVO
    )


@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespeopleListEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "List does not exist"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            sales_people = Salesperson.objects.create(**content)
            return JsonResponse(
                sales_people,
                encoder=SalespeopleListEncoder,
                safe=False,
            )
        except Exception as e:
            return JsonResponse(
                {"message": str(e)},
                status=404,
            )


@require_http_methods(["DELETE"])
def api_show_sales_person(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespeopleListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "id does not exist"},
                status=404,
                )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomersListEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "List does not exist"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomersListEncoder,
                safe=False,
            )
        except Exception as e:
            return JsonResponse(
                {"message": str(e)},
                status=500,
            )


@require_http_methods(["DELETE"])
def api_show_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomersListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "id does not exist"},
                status=404,
                )


@require_http_methods(["GET", "POST"])
def api_list_sales(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sales = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )

        data = json.dumps({"sold": True})
        headers = {"Content-Type": "application/json"}
        response = requests.put(f'http://project-beta-inventory-api-1:8000/api/automobiles/{automobile_vin}/', data=data, headers=headers)
        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            encoder=SalesListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_show_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesListEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "id does not exist"},
                status=404,
                )
