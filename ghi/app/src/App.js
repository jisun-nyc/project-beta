import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import ServiceAppointmentList from './ServiceAppointmentList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleList from './VehicleList';
import VehicleForm from './VehicleForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import './index.css';
import SalespeopleList from './SalespeopleList.js'
import SalespersonForm from './SalespersonForm.js'
import CustomersList from './CustomersList.js'
import CustomerForm from './CustomerForm.js'
import SalesList from './SalesList.js'
import SaleForm from './SaleForm.js'
import SalespersonHistoryList from './SalespersonHistoryList.js'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<ServiceAppointmentList />} />
            <Route path="new" element={<ServiceAppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleList />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm/>} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList /> } />
            <Route path="new" element={<CustomerForm /> } />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList /> } />
            <Route path="new" element={<SaleForm /> } />
            <Route path="history" element={<SalespersonHistoryList /> } />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
