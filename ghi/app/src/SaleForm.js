import React, {useEffect, useState } from 'react';

function SaleForm() {
    const [autos, setAutos] = useState([]);
    const [salesPeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })


        const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const filteredautos = data.autos.filter(auto => auto.sold === false)
            setAutos(filteredautos);
        }
    }


        const fetchSalespeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }


    const fetchCustomersData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = `http://localhost:8090/api/sales/`;

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
            setAutos(autos.filter(auto => auto.sold == false));
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
        ...formData,

        [inputName]: value
        });
    }


    useEffect(() => {
        fetchData();
        fetchSalespeopleData();
        fetchCustomersData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <select value={formData.automobile}
                                onChange={handleFormChange}
                                required name="automobile"
                                id="automobile" className="form-select"
                            >
                            <option value="">Choose an automobile VIN...</option>
                            {autos.map(auto => {
                            return (
                                <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                            )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={formData.salesperson}
                                onChange={handleFormChange}
                                required name="salesperson"
                                id="salesperson" className="form-select"
                            >
                            <option value="">Choose a salesperson...</option>
                            {salesPeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={formData.customer}
                                onChange={handleFormChange}
                                required name="customer"
                                id="customer" className="form-select"
                            >
                            <option value="">Choose a customer</option>
                            {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.price} onChange={handleFormChange}
                                placeholder="Price" required type="number"
                                name="price" id="price"
                                className="form-control"
                            />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SaleForm;
