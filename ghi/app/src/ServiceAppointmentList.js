import React, { useState, useEffect } from 'react';


function ServiceAppointmentList() {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        async function loadAppointments() {
            const response = await fetch('http://localhost:8080/api/appointments/');

            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments.filter(appointment => appointment.status == "scheduled"));
            } else {
                console.error(response);
            };
        };

        loadAppointments();
    }, []);


    const handleCancel = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method:"PUT",
            headers:{
                "Content-Type": "application/json",
            }
        }
        const response = await fetch (url, fetchConfig)
        if (response.ok) {
            const result = appointments.filter(appointment => appointment.id != id);
            setAppointments(result);
        };
    }


    const handleFinish = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/finish/`
        const fetchConfig = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const result = appointments.filter(appointment => appointment.id != id);
            setAppointments(result);
        };
    }


    return (
        <>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ String(appointment.is_vip) }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date }</td>
                            <td>{ appointment.time }</td>
                            <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => handleCancel(appointment.id)}>Cancel</button>
                            </td>
                            <td>
                                <button className="btn btn-success" onClick={() => handleFinish(appointment.id)}>Finish</button>
                            </td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}


export default ServiceAppointmentList;
