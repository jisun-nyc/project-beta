import React, { useEffect, useState } from 'react';


function ServiceAppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technicianId, setTechnicianId] = useState(0);
    const [reason, setReason] = useState('');


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }


    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }


    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }


    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }


    const handleTechnicianIdChange = (event) => {
        const value = event.target.value;
        setTechnicianId(value);
    }


    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date = date;
        data.time = time;
        data.technician_id = technicianId;
        data.reason = reason;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTechnicianId('');
            setReason('');

        };
    }


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        };
    }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                        <input value={vin} onChange={handleVinChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={customer} onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                        <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={date} onChange={handleDateChange} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                        <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={time} onChange={handleTimeChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                        <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                        <select value={technicianId} onChange={handleTechnicianIdChange} required id="technician_id" name="technician_id" className="form-select">
                            <option>Choose a technician</option>
                            {technicians.map(tech => {
                                return (
                                    <option key={tech.id} value={tech.id}>
                                        {tech.first_name} {tech.last_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={reason} onChange={handleReasonChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                        <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default ServiceAppointmentForm;
