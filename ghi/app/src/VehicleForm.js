import React, { useState, useEffect } from 'react';


function VehicleForm() {
    const [modelName, setModelName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturerId, setManufacturerId] = useState('');
    const [manufacturers, setManufacturers] = useState([]);


    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };


    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    };


    const handleManufacturerIdChange = (event) => {
        const value = event.target.value;
        setManufacturerId(value);
    };


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = modelName;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturerId;

        const vehicleUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(vehicleUrl, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json();

            setModelName('');
            setPictureUrl('');
            setManufacturerId('');
        };
    };


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Vehicle Model </h1>
                    <form onSubmit={handleSubmit} id="create-vehicle-form">
                        <div className="form-floating mb-3">
                            <input value={modelName} onChange={handleModelNameChange} placeholder="Model name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture url</label>
                        </div>
                        <div className="mb-3">
                            <select value={manufacturerId} onChange={handleManufacturerIdChange} required id="manufacturer" name="manufacturer" className="form-select">
                                <option>Choose a manufacturer</option>
                                {manufacturers.map(manu => {
                                    return (
                                        <option key={manu.id} value={manu.id}>
                                            {manu.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default VehicleForm;
