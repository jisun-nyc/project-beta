import React, { useState, useEffect } from 'react';


function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);


    async function loadAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        } else {
            console.error(response);
        };
    };


    useEffect(() => {
        loadAutomobiles();
    }, []);


    return (
        <>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(automobile => {
                        return (
                        <tr key={automobile.href}>
                            <td>{ automobile.vin }</td>
                            <td>{ automobile.color }</td>
                            <td>{ automobile.year }</td>
                            <td>{ automobile.model.name }</td>
                            <td>{ automobile.model.manufacturer.name }</td>
                            <td>{ String(automobile.sold) }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}


export default AutomobileList;
