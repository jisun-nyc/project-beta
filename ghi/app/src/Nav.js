import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers">Manufacturer List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/new/">Create a Manufacturer</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Models
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                <NavLink className="nav-link" to="/models">Model List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/models/new/">Create a Model</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles">Automobile List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/new/">Create an Automobile</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                <NavLink className="nav-link" to="/technicians">Technician List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/new/">Create a Technician</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                <NavLink className="nav-link" to="/appointments">Appointment List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/new/">Create an Appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/history/">Appointment History</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Salepeople
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/new/">Add a Salesperson</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                <NavLink className="nav-link" to="/customers">Customer List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/new/">Add a Customer</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </button>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                <li className="nav-item">
                <NavLink className="nav-link" to="/sales">Sales List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/new/">Add a Sale</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/history/">Salesperson History</NavLink>
                </li>
              </ul>
            </div>
          </ul>
          <form className="d-flex">
            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
            <button className="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
