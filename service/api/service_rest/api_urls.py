from django.urls import path
from .views import (
    api_list_technicians,
    api_delete_technicians,
    api_list_appointments,
    api_delete_appointments,
    api_cancel_appointments,
    api_finish_appointments
)


urlpatterns = [
    path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians"
    ),

    path(
        "technicians/<int:id>/",
        api_delete_technicians,
        name="api_show_technicians"
    ),

    path(
        "appointments/",
        api_list_appointments,
        name="api_list_appointments"
    ),

    path(
        "appointments/<int:id>/",
        api_delete_appointments,
        name="api_delete_appointments"
    ),

    path(
        "appointments/<int:id>/cancel/",
        api_cancel_appointments,
        name="api_cancel_appointments"
    ),

    path(
        "appointments/<int:id>/finish/",
        api_finish_appointments,
        name="api_cancel_appointments"
    ),
]
