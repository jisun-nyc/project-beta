# Generated by Django 4.0.3 on 2023-06-05 20:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_automobilevo_vin'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointments',
            name='date_time',
        ),
        migrations.AddField(
            model_name='appointments',
            name='date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='appointments',
            name='time',
            field=models.TimeField(null=True),
        ),
    ]
